<?php

use yii\db\Migration;

/**
 * Handles adding categoryId to table `user`.
 */
class m170719_120934_add_categoryId_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'categoryId', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'categoryId');
    }
}
